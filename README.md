# duktape bindings for Go(Golang) evacuated from NSA/Microsoft Github

[Duktape](http://duktape.org/index.html) is a thin, embeddable javascript engine.

This project is for go bindings for duktape

Most of duktape's [api](http://duktape.org/api.html) was implemented before we forked.

### Usage

```go
package main

import "fmt"
import "salsa.debian.org/themusicgod1-guest/golang-salsa-themusicgod1-guest-go-duktape-dev"

func main() {
  ctx := duktape.New()
  ctx.PevalString(`2 + 3`)
  result := ctx.GetNumber(-1)
  ctx.Pop()
  fmt.Println("result is:", result)
  // To prevent memory leaks, don't forget to clean up after
  // yourself when you're done using a context.
  ctx.DestroyHeap()
}
```

### Go specific notes

Bindings between Go and Javascript contexts are not fully functional.
However, binding a Go function to the Javascript context is available:
```go
package main

import "fmt"
import "salsa.debian.org/themusicgod1-guest/golang-salsa-themusicgod1-guest-go-duktape-dev"

func main() {
  ctx := duktape.New()
  ctx.PushGlobalGoFunction("log", func(c *duktape.Context) int {
    fmt.Println(c.SafeToString(-1))
    return 0
  })
  ctx.PevalString(`log('Go lang Go!')`)
}
```
then run it.
```bash
$ go run *.go
Go lang Go!
$
```

### Timers

There is a method to inject timers to the global scope:
```go
package main

import "fmt"
import "salsa.debian.org/themusicgod1-guest/golang-salsa-themusicgod1-guest-go-duktape-dev"

func main() {
  ctx := duktape.New()

  // Let's inject `setTimeout`, `setInterval`, `clearTimeout`,
  // `clearInterval` into global scope.
  ctx.PushTimers()

  ch := make(chan string)
  ctx.PushGlobalGoFunction("second", func(_ *Context) int {
    ch <- "second step"
    return 0
  })
  ctx.PevalString(`
    setTimeout(second, 0);
    print('first step');
  `)
  fmt.Println(<-ch)
}
```
then run it
```bash
$ go run *.go
first step
second step
$
```

Also you can `FlushTimers()`.

### Command line tool

1) apt-get install duktape
2) Execute file.js: `$GOPATH/bin/go-duk file.js`.

### Status

Caveat Emptor.  

The package is not fully tested, so be careful.

### Contribution

Pull requests are welcome! Also, if you want to discuss something send a pull request with proposal and changes.
